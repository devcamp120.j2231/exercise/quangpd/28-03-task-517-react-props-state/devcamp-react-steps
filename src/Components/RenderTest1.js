import { Component } from "react";

class RenderTest1 extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <>
                <div>
                    <ul>
                        {this.props.steps.map((item,index)=>{
                            return <li key={index}>Bước . {item.id} {item.title} : {item.content}</li>
                        })}
                    </ul>
                </div>
            </>
        )
    }
}

export default RenderTest1
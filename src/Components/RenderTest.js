
export default function({data}){

    return(<>
        <div>
                <ul>
                    {data.map((item,index) =>{
                        return <li key={index}>Bước .{item.id} {item.title} : {item.content}</li>
                    })}
                </ul>
            </div>
        </>
    )
}